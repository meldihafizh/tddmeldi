from django.db import models

class modelStatus(models.Model):
	status = models.TextField(max_length=300)
	time = models.DateTimeField(auto_now=True)
