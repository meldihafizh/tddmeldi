from django.urls import include, path
from . import views
from django.shortcuts import render
from django.conf.urls import url

app_name = 'tdd'
urlpatterns = [
	path('status/',views.home,name='index'),
	path('books/',views.search_books,name='books'),
	path('',views.main,name='main'),
	path('landing/',views.landing,name='landing'),
]