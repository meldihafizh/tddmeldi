function grayScaleTheme(){
	var body2 = document.getElementById('body');
	body2.classList.toggle('inverted');
}

$(document).ready(function(){
	var allPanels = $('.accordion > ul').hide();

	$('.accordion > dt > a').click(function(){
		if ($(this).parent().next().is(':visible')) {
			allPanels.slideUp();
		}
		else{
			allPanels.slideUp();
			$(this).parent().next().slideDown();
		}
		return false;
	});
});
$(function(){
		var $result = $('#result');
		$.ajax({
		type: 'GET',
		url: 'https://www.googleapis.com/books/v1/volumes?q=galau',
		datatype: "json",
		success: function(data){
			for(a=0;a<data.items.length;a++){
				appended = '<tr>'+'<td>'+data.items[a].volumeInfo.title+'</td>'
				if (data.items[a].volumeInfo.authors !== undefined) {
					appended += '<td>'+data.items[a].volumeInfo.authors+'</td>'
				}
				else{
					appended += '<td>-</td>'
				}
				if (data.items[a].volumeInfo.imageLinks !== undefined) {
					appended += '<td>'+'<img src="'+data.items[a].volumeInfo.imageLinks.thumbnail+'"></tr>'
				}
				else{
					appended += '<td>-</td></tr>'
				}
				$result.append(appended)
			}
		}
	});
});

$(function(){
	$('#submit').click(function(){
		var searchResult = $('#search').val();
		var $result = $('#result');
		$result.empty();
		$.ajax({
			type: 'GET',
			url: 'https://www.googleapis.com/books/v1/volumes?q='+searchResult,
			datatype: "json",
			success: function(data){
				console.log(data.items)
				for(i=0;i<data.items.length;i++){
					
					appended = '<tr>'+'<td>'+data.items[i].volumeInfo.title+'</td>'
					
					if (data.items[i].volumeInfo.authors !== undefined) {
						appended += '<td>'+data.items[i].volumeInfo.authors+'</td>'
					}
					else{
						appended += '<td>-</td>'
					}
					if (data.items[i].volumeInfo.imageLinks !== undefined) {
						appended += '<td>'+'<img src="'+data.items[i].volumeInfo.imageLinks.thumbnail+'"></tr>'
					}
					else{
						appended += '<td>-</td></tr>'
					}

					$result.append(appended)
				}
			}
		});

	});
});

// $(function(){

// })
